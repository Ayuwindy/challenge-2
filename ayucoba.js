//Nomer 1//

function changeWord(selectedText, changeText, text) {
        return text.replace(selectedText, changeText, text);}  
        
        const kalimat1 = "Andini sangat mencintai kamu selamanya"
        const kalimat2 = "Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu"
        
        console.log(changeWord('mencintai', 'membenci', kalimat1))
        console.log(changeWord('bromo', 'semeru', kalimat2))
        
//Nomor2//
        function checkTypeNumber(givenNumber) {
            if(givenNumber%2==0)
            {return ("bilangan genap");} 
            else
            {return ("bilangan ganji");}
        }
        console.log(checkTypeNumber(10))
        console.log(checkTypeNumber(3))
        console.log(checkTypeNumber('3'))
        console.log(checkTypeNumber({})
        console.log(checkTypeNumber([])
        console.log(checkTypeNumber())
        
//Nomor3//
                function cekemail(email){
                    if(email.indexOf("@")!=-1 && email.indexOf(".")!=-1){
                        return ("VALID");
                    }
                    else{
                        return ("INVALID");
                    }
                }
                
        console.log(checkEmail('apranata@binar.co.id'))
        console.log(checkEmail('apranata@binar.com'))
        console.log(checkEmail('apranata@binar.co.id'))
        console.log(checkEmail('apranata@binar'))
        console.log(checkEmail('apranata'))
        console.log(checkEmail())
        
        
//Nomor 4//
function isValidPassword(sandi) {
        if (sandi.match (/[a-z]/g) && sandi.match(
                /[A-Z]/g) && sandi.match(
                /[0-9]/g) && sandi.match(
                /[^a-zA-Z\d]/g) && sandi.length >= 8){
                        return true
                }
                
                else {
                return false
                }
            }
          console.log(isValidPassword("Meong2021"))
        
//Nomor5//
        function getSplitName(personName){ 
          const arrPersonName = personName.split(" ")
        
            return `First Name '${arrPersonName[0]}'
        Middle Name '${arrPersonName[1]}'
        Last Name '${arrPersonName[2]}'`
        }
            console.log(getSplitName("Aldi Daniela Pranata"))
            console.log(getSplitName("Dwi Kuncoro"))
            console.log(getSplitName("Aurora"))
            console.log(getSplitName("Aurora Aureliya Sukma Darma"))
            console.log(getSplitName(0))
        
        
//Nomor 6//
const dataAngka = [9,4,7,7,4,3,2,2,8];

function getAngkaTerbesarKedua (dataNumbers) {
    if (dataNumbers != null) {
        var getAngkaTerbesarKedua = dataNumbers[8];

        for (let i = 8; i < dataNumbers.length; i++) {
            if (dataNumbers[i] > getAngkaTerbesarKedua) {
                getAngkaTerbesarKedua = dataNumbers[i];    
            } 
            return getAngkaTerbesarKedua
        }
        if (dataNumbers != dataAngka) {
            return "Error: tipe data sudah berupa number namun valuenya  tidak termasuk variabel dataAngka "
        }
    } else {
        return "Error: tipe data berupa undefined alias belum diisi valuenya harusnya data number dan diisi value "
    }
}


console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());


//Nomor 7//
const dataPenjualanPakAldi = [{
        namaProduct: 'Sepatu Futsal Nike Vapor Academy 8',
        hargaSatuan: 760000,
        kategori: "Sepatu Sport",
        totalTerjual: 90,
     },
     {
        namaProduct: 'Sepatu Warrior Tristan Black Brown High',
        hargaSatuan: 960000,
        kategori: "Sepatu Sneaker",
        totalTerjual: 37,
     },
     {
        namaProduct: 'Sepatu Warrior Tristan Maroon High ',
        kategori: "Sepatu Sneaker",
        hargaSatuan: 360000,
        totalTerjual: 90,
     },
     {
        namaProduct: 'Sepatu Warrior Rainbow Tosca Corduroy',
        hargaSatuan: 120000,
        kategori: "Sepatu Sneaker",
        totalTerjual: 90,
     }
  ]
  
  function getTotalPenjualan(arr){
     let total = 0
     arr.forEach(item => {
        total += item.totalTerjual
     });
     return total
  }
  
  
  console.log(getTotalPenjualan(dataPenjualanPakAldi))
        
        //nomor 8//
        const dataPenjualanNovel = [
            {
              idProduct: 'BOOK002421',
              namaProduk: 'Pulang - Pergi',
              penulis: 'Tere Liye',
              hargaBeli: 60000,
              hargaJual: 86000,
              totalTerjual: 150,
              sisaStok: 17,
            },
            {
              idProduct: 'BOOK002351',
              namaProduk: 'Selamat Tinggal',
              penulis: 'Tere Liye',
              hargaBeli: 75000,
              hargaJual: 103000,
              totalTerjual: 171,
              sisaStok: 20,
            },
            {
              idProduct: 'BOOK002941',
              namaProduk: 'Garis Waktu',
              penulis: 'Fiersa Besari',
              hargaBeli: 67000,
              hargaJual: 99000,
              totalTerjual: 213,
              sisaStok: 5,
            },
            {
              idProduct: 'BOOK002941',
              namaProduk: 'Laskar Pelangi',
              penulis: 'Andrea Hirata',
              hargaBeli: 55000,
              hargaJual: 68000,
              totalTerjual: 20,
              sisaStok: 56,
            },
          ];
          var TotalKeuntungan = dataPenjualanNovel.filter((item) =>
          item.namaProduk === 'Pulang - Pergi' || item.namaProduk === 'Selamat Tinggal' || item.namaProduk === 'Garis Waktu' || 
          item.namaProduk === 'Laskar Pelangi'
      ).reduce((accumulator, item) => {
          return accumulator + (item.hargaJual-item.hargaBeli) * (item.totalTerjual+item.sisaStok);
      }, 0)
      
      
      var TotalModal = dataPenjualanNovel.filter((item) =>
          item.namaProduk === 'Pulang - Pergi' || item.namaProduk === 'Selamat Tinggal' || item.namaProduk === 'Garis Waktu' || 
          item.namaProduk === 'Laskar Pelangi'
      ).reduce((accumulator, item) => {
          return accumulator + (item.hargaBeli) * (item.totalTerjual+item.sisaStok);
      }, 0)
      
      
      var ProdukBukuTerlaris = dataPenjualanNovel.filter((item) => item.totalTerjual > 171).map(item => item.namaProduk);
      
      var PenulisTerlaris = dataPenjualanNovel.filter((item) => item.totalTerjual > 171).map(item => item.penulis);
      
      
      function hitungTotalPenjualan(dataPenjualan) {
          dataPenjualan1 = TotalKeuntungan;
          dataPenjualan2 = TotalModal;
          dataPenjualan3 = ProdukBukuTerlaris;
          dataPenjualan4 = PenulisTerlaris;
      
          return `{
              totalKeuntungan: Rp. ${dataPenjualan1},
              totalModal: Rp. ${dataPenjualan2},
              produkBukuTerlaris: ${dataPenjualan3},
              penulisTerlaris: ${dataPenjualan4}
          }`
      }
      
      console.log(hitungTotalPenjualan(dataPenjualanNovel))
      